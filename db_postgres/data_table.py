from config_data.bot_instance import bot
from db_postgres.conn_postg import cur, conn


# Обновляем запись в таблице seancerequest с выбранной датой и временем
def process_selected_time(message, chosen_date, session_id):
    chosen_time = message.text
    try:
        # Обновляем запись в таблице seancerequest с выбранной датой и временем
        update_query = "UPDATE seancerequest SET start_time = %s, telegram_notification_sent = TRUE WHERE id = %s;"
        cur.execute(update_query, (f"{chosen_date} {chosen_time}", session_id))
        conn.commit()

        # Отправляем пользователю сообщение с подтверждением
        bot.send_message(
            message.chat.id,
            f"Вы предложили дату: {chosen_date.strftime('%d.%m.%Y')} и время: {chosen_time}",
        )
    except Exception as e:
        # Если произошла ошибка при выполнении запроса, отправляем сообщение об ошибке пользователю
        print(f"Ошибка: {e}")
        bot.send_message(
            message.chat.id, "Произошла ошибка. Пожалуйста, попробуйте еще раз."
        )
