# from research_website.data_website import (
#     send_session_notifications,
#     send_session_notification_to_users,
#     get_telegram_bot_id_from_db,
# )
from handlers.start_handlers import handle_start
from handlers.other_Handl_butt import handle_agree, handle_decline, handle_suggest_date
from config_data.bot_instance import bot


# send_session_notifications()

# send_session_notification_to_users()

# get_telegram_bot_id_from_db()


@bot.message_handler(commands=["start"])
def start_message(message):
    handle_start(message)


@bot.message_handler(func=lambda message: message.text == "Согласиться")
def agree_message(message):
    handle_agree(message)


@bot.message_handler(func=lambda message: message.text == "Отказаться")
def decline_message(message):
    handle_decline(message)


@bot.message_handler(func=lambda message: message.text == "Предложить другую дату")
def suggest_date_message(message):
    handle_suggest_date(message)


bot.polling(none_stop=True)
