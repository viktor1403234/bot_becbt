import datetime
from config_data.bot_instance import bot
from keyboards.time_keyboard import generate_time_keyboard
from handlers.other_Handl_butt import handle_suggest_date


def process_date(message):
    try:
        chosen_date = datetime.datetime.strptime(message.text, "%d.%m.%Y").date()
        today = datetime.date.today()
        if chosen_date < today:
            bot.send_message(
                message.chat.id,
                "Выбранная дата находится в прошлом. Пожалуйста, выберите корректную дату.",
            )
            handle_suggest_date(message)
        else:
            time_markup = generate_time_keyboard()
            msg = bot.send_message(
                message.chat.id, "Выберите время:", reply_markup=time_markup
            )
            bot.register_next_step_handler(
                msg, lambda message: process_selected_time(message, chosen_date)
            )
    except ValueError:
        bot.send_message(
            message.chat.id,
            "Некорректный формат даты. Пожалуйста, используйте формат ДД.ММ.ГГГГ (например, 23.10.2023).",
        )
        handle_suggest_date(message)


def process_selected_time(message, chosen_date):
    chosen_time = message.text

    bot.send_message(
        message.chat.id,
        f"Вы предложили дату: {chosen_date.strftime('%d.%m.%Y')} и время: {chosen_time}",
    )
