from config_data.bot_instance import bot
from keyboards.buttons_menu import generate_keyboard


@bot.message_handler(commands=["start"])
def handle_start(message):
    keyboard = generate_keyboard()

    bot.send_message(message.chat.id, "Выберите действие:", reply_markup=keyboard)
