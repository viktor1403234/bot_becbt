from config_data.bot_instance import bot
from research_website.post_agree import send_post_request
from research_website.post_refuse import send_post_request


@bot.message_handler(func=lambda message: message.text == "Согласиться")
def handle_agree(message):
    bot.send_message(message.chat.id, "Вы согласились.")
    data_to_send = {"key": "value"}
    send_post_request(data_to_send)


@bot.message_handler(func=lambda message: message.text == "Отказаться")
def handle_decline(message):
    bot.send_message(
        message.chat.id,
        "Вы отказались. Вы можете выбрать другого специалиста на сайте: https://becbt.online/new-persons-list",
    )

    data_to_send = {"key": "value"}

    send_post_request(data_to_send)


@bot.message_handler(func=lambda message: message.text == "Предложить другую дату")
def handle_suggest_date(message):
    from date_time.time_date import process_date

    msg = bot.send_message(message.chat.id, "Выберите дату (ДД.ММ.ГГГГ):")
    bot.register_next_step_handler(msg, process_date)
