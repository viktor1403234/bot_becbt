import datetime
import requests
from bs4 import BeautifulSoup
import schedule

from db_postgres.conn_postg import cur
from config_data.bot_instance import bot
from config_data.config import SITE_URL


def send_session_notifications():
    response = requests.get(SITE_URL)
    soup = BeautifulSoup(response.text, "html.parser")

    sessions = soup.find_all("div", class_="session")

    for session in sessions:
        full_name = session.find("span", class_="name").text
        datetime_str = session.find("span", class_="datetime").text
        datetime_obj = datetime.datetime.strptime(datetime_str, "%d.%m.%Y %H:%M")

        send_session_notification_to_users(full_name, datetime_obj)


def send_session_notification_to_users(full_name, datetime_obj):
    cur.execute("SELECT firstname, lastname, middlename, telegram_bot_id FROM person")
    for row in cur.fetchall():
        firstname, lastname, middlename, telegram_bot_id = row
        message = (
            f"{firstname} {lastname} запросил индивидуальный сеанс на {datetime_obj}"
        )
        bot.send_message(telegram_bot_id, message)


schedule.every(1).hours.do(send_session_notifications)


def get_telegram_bot_id_from_db(client_id):
    try:
        cur.execute("SELECT telegram_bot_id FROM person WHERE id = %s;", (client_id,))
        telegram_bot_id = cur.fetchone()[0]
        return telegram_bot_id
    except Exception as e:
        print("Ошибка при извлечении telegram_bot_id из базы данных:", e)
        return None
