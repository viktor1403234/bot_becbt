import requests


def send_post_request(data):
    url = "https://example.com/api"
    try:
        response = requests.post(url, data=data)
        response.raise_for_status()
        print("POST запрос успешно отправлен.")
        return response.text
    except requests.exceptions.HTTPError as errh:
        print(f"HTTP Error: {errh}")
    except requests.exceptions.ConnectionError as errc:
        print(f"Error Connecting: {errc}")
    except requests.exceptions.Timeout as errt:
        print(f"Timeout Error: {errt}")
    except requests.exceptions.RequestException as err:
        print(f"Ooops, Something Else: {err}")
