from telebot import types


def generate_keyboard():
    keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True)
    agree_button = types.KeyboardButton("Согласиться")
    decline_button = types.KeyboardButton("Отказаться")
    suggest_date_button = types.KeyboardButton("Предложить другую дату")
    keyboard.add(agree_button, decline_button, suggest_date_button)
    return keyboard
