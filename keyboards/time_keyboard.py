from telebot import types


def generate_time_keyboard():
    markup = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    hours = [str(i).zfill(2) for i in range(24)]
    minutes = [str(i).zfill(2) for i in range(0, 60, 10)]
    for hour in hours:
        row = []
        for minute in minutes:
            row.append(f"{hour}:{minute}")
        markup.add(*row)
    return markup
